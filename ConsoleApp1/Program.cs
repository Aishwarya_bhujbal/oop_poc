﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
   
    class PersonalInfo 
    {
        public int employeeId = 101;
        public string name = "Aparna";
        public virtual void showInfo()
        {
            Console.WriteLine("Employee ID : " + employeeId);
            Console.WriteLine("Name : " + name);
            
        }

        
    }
    class Program : PersonalInfo
    {

        private int employeeId { get; set; }
        private string name { get; set; }
        private string profile { get; set; }
        private double salary { get; set; }
        private int rating { get; set; }
        public override void showInfo()
        {
            Console.WriteLine("Employee ID : " +employeeId);
            Console.WriteLine("Name : " +name);
            Console.WriteLine("Profile : " + profile);
            Console.WriteLine("Salary : " + salary);
            Console.WriteLine("Rating : " + rating);
        }
        void Increment()
        {
            if (rating > 6)
            {
                salary = salary + 10000;
                Console.WriteLine("Salary of " + name + " is " + salary + " now!");
            }
            Console.WriteLine("Salary is unchanged!!!");   
            
        }
        
        static void Main(string[] args)
        {
            char ch;
            do
            {
                Console.WriteLine("Enter ID, Name, Profile , salary and rating : ");
                Program p = new Program()
                {
                    employeeId = int.Parse(Console.ReadLine()),
                    name = Console.ReadLine(),
                    profile = Console.ReadLine(),
                    salary = double.Parse(Console.ReadLine()),
                    rating = int.Parse(Console.ReadLine())
                };

                back:
                Console.WriteLine("\n Select option (1/2/3):\n 1) Show Added Information\n 2)show Increment\n 3) Exit :");
                int option = int.Parse(Console.ReadLine());


                if (option == 1)
                {
                    p.showInfo();
                    goto back;
                }
                else if (option == 2)
                {
                    p.Increment();
                    goto back;
                }
                else 
                {
                    Console.WriteLine("Do you wish to continue? (y/n)");
                    ch = char.Parse(Console.ReadLine());
                }
                    
                
            } while (ch == 'y');       
           
            
           
          
            Console.ReadKey();
        }
    }
}
